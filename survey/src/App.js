import React, { Component } from 'react'
import logo from './question-mark.png'
import './App.css'

import io from 'socket.io-client'
const R = require('ramda')

const getSurvey = surveyid => R.find(R.propEq('id', surveyid))

class App extends Component {
  constructor(props) {
    super(props)

    this.handleItemClick = this.handleItemClick.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    this.state = {}
  }
  componentDidMount() {
    this.socket = io.connect('http://localhost:3001', {
        transports: ['websocket'],
      forceNew: true
    })
    this.socket.on('connect', () => {
      console.log('connected')
    })
    this.socket.on('surveys', (...data) => {
      console.log('whhhhhhaaaaat?????????', data)
      this.setState({ surveys: data })
    })
  }
  handleItemClick(ev) {
    this.setState({ surveyId: Number(ev.target.dataset.surveyid) })
  }
  handleInputChange(ev) {
    this.setState({ inputValue: ev.target.value })
  }
  handleSubmit() {
    console.log('submit', this.state.inputValue)
    this.socket.emit(
      'new_question',
      {
        surveyId: this.state.surveyId,
        question: this.state.inputValue
      }
    )
    this.setState({ inputValue: '' })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">AnonimOS</h1>
        </header>

        { this.state.surveys && !this.state.surveyId &&
          <div id='surveys'>
            <h1>Surveys</h1>
            <ul>
              { this.state.surveys.map(survey =>
                <li
                  key={ survey.id }
                  data-surveyid={ survey.id }
                  onClick={ this.handleItemClick }>
                  { survey.name }
                </li>
              )}
            </ul>
          </div>
        }

        { this.state.surveyId && this.renderSurvey(this.getSurvey()) }
      </div>
    )
  }
  renderSurvey(survey) {
    return <div id='survey'>
      <h1>{ survey.name }</h1>
      <div id='questions'>
        <input type='text' onChange={this.handleInputChange} />
        <button onClick={this.handleSubmit}>Send</button>
      </div>
    </div>
  }
  getSurvey() {
    return getSurvey(this.state.surveyId)(this.state.surveys)
  }
}

export default App
