import React from 'react'
import ReactDOM from 'react-dom'

import { Server, SocketIO as mockSocketIO } from 'mock-socket'

jest.mock('socket.io-client', () => mockSocketIO)

import App from './App'

import sinon from 'sinon'
import Enzyme, { mount, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({ adapter: new Adapter() })


// it('renders without crashing', () => {
//   const div = document.createElement('div')
//   ReactDOM.render(<App />, div)
//   ReactDOM.unmountComponentAtNode(div)
// })

const mockServer = new Server('http://localhost:3001')
mockServer.on('connection', () => {
  mockServer.emit('connect')
})

describe('what?', () => {
  beforeEach(() => {
    jest.useFakeTimers()
  })

  afterEach(() => {
    jest.useRealTimers()
    jest.clearAllMocks()
  })

  it('renders surveys', () => {
    const wrapper = mount(<App />)

    setTimeout(() => {
      mockServer.emit('surveys', 
        { id: 1, name: 'Survey 1' },
        { id: 2, name: 'Survey 2' },
        // { id: 3, name: 'Survey 3' }
      )
      wrapper.update()
    }, 0)

    jest.runOnlyPendingTimers()

    expect(wrapper.find('#surveys')).toHaveLength(1)
    expect(wrapper.find('li').length).toEqual(3)
  })
})
