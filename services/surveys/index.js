const app = require('express')()
const http = require('http').Server(app)
const io = require('socket.io')(http)
const _ = require('lodash')
const redis = require("redis")
const subscriber = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST)
const store = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST)

store.keys('*', (err, keys) => {

  if (err) {
    console.log('Unable to retrieve redis keys')
    return
  }

  console.log('keys retrieved', keys)

  subscriber.on("message", (channel, message) => {
    const satelliteParameter = fromRedisChannel(channel)
    io.to(satelliteParameter).emit('param_update', {
      param: satelliteParameter, 
      value: JSON.parse(message)
    })
  })
  
  io.on('connection', socket => {
    console.log('New client with id', socket.id)
    socket.emit('surveys', [
      { id: 1, name: 'Survey 1' },
      { id: 2, name: 'Survey 2' },
      { id: 3, name: 'Survey 3' }
    ])
    socket.on('new_question', (data, cb) => {
      console.log('new_question', data)
    })
  })
  
  const PORT = process.env.PORT || 3000
  http.listen(PORT, () => {
    console.log('listening on *: ' + PORT)
  })

})
